from mesa import Agent, Model
from mesa.time import RandomActivation
import matplotlib.pyplot as plt
from mesa.space import MultiGrid
import numpy as np
from mesa.datacollection import DataCollector


def compute_gini(model):
    agent_wealths = [agent.wealth for agent in model.schedule.agents]
    x = sorted(agent_wealths)
    N = model.num_agents
    B = sum( xi * (N-i) for i,xi in enumerate(x) ) / (N*sum(x))
    return (1 + (1/N) - 2*B)

class MoneyAgent(Agent):
    """ An agent with fixed initial wealth."""
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.wealth = 1

    def move(self): #next
        possible_steps = self.model.grid.get_neighborhood(
            self.pos,
            moore=True,
            include_center=False)
        new_position = self.random.choice(possible_steps)
        self.model.grid.move_agent(self, new_position)

    def give_money(self): #see, then act
        cellmates = self.model.grid.get_cell_list_contents([self.pos]) #see
        if len(cellmates) > 1:
            other = self.random.choice(cellmates)
            #added functionality for robin-hood money taking
            if self.wealth == 0 and other.wealth > 0:
                self.wealth = other.wealth
                other.wealth = 0
            elif self.wealth > other.wealth: #agents are kind and give money to poorer agents
                other.wealth += 1
                self.wealth -= 1
            else: #but they are also robin hood and take money if they choose a wealthier agent
                other.wealth -= 1
                self.wealth += 1

    def step(self): #act
        self.move()
        #if self.wealth > 0: #in the original code, agents would only give_money when they had wealth
        #in my variation, agents can also "steal" money if they have less wealth
        self.give_money()

class MoneyModel(Model):
    """A model with some number of agents."""
    def __init__(self, N, width, height):
        self.num_agents = N
        self.grid = MultiGrid(width, height, True)
        self.schedule = RandomActivation(self)

        # Create agents
        for i in range(self.num_agents):
            a = MoneyAgent(i, self)
            self.schedule.add(a)
            # Add the agent to a random grid cell
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            self.grid.place_agent(a, (x, y))

        self.datacollector = DataCollector(
            model_reporters={"Gini": compute_gini},
            agent_reporters={"Wealth": "wealth"})

    def step(self):
        self.datacollector.collect(self)
        self.schedule.step()


model = MoneyModel(50, 10, 10)
for i in range(100):
    model.step()

agent_wealth = model.datacollector.get_agent_vars_dataframe()
end_wealth = agent_wealth.xs(99, level="Step")["Wealth"]
end_wealth.hist(bins=range(agent_wealth.Wealth.max()+1))
plt.show()


